package com.nerdcorn.myth.model.user;

import com.nerdcorn.myth.model.AbstractModel;
import com.nerdcorn.myth.model.attribute.MythAttribute;
import sun.security.util.Password;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class User extends AbstractModel {
    /**
     * Standard hardcode attributes
     */
    private UUID id;
    private String name;
    private Password password;
    private Date created;

    private ArrayList<MythAttribute> attributeList = new ArrayList<>();
    private ArrayList<UUID> channels = new ArrayList<>();
}
