package com.nerdcorn.myth.model.channel;

import com.nerdcorn.myth.model.AbstractModel;
import com.nerdcorn.myth.model.user.User;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class Channel extends AbstractModel {
    private UUID id;
    private String name;
    private ArrayList<ChannelBuffer> buffer = new ArrayList<>();
    private ArrayList<User> usersConnected = new ArrayList<>();
    private ArrayList<User> usersBanned = new ArrayList<>();
    private String uniqId;
    private User creator;
    private String description;
    private String title;
    private String alias;
    private String colour;
    private Boolean closed;

    public Channel() {
    }

    public Channel(String name, User creator) {
        this.id = UUID.randomUUID();
        setName(name)
        setCreator(creator);
        setClosed(true);
    }

    public UUID getId() {
        return this.id;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public Boolean getClosed() {
        return this.closed;
    }
}
