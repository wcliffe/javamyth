package com.nerdcorn.myth.model.channel;

import com.nerdcorn.myth.model.AbstractModel;
import com.nerdcorn.myth.model.user.User;

import java.util.UUID;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class ChannelBuffer extends AbstractModel {
    private StringBuilder output = null;
    private User user;
    private UUID ref;

    public ChannelBuffer(User user, UUID ref) {
        this.user = user;
        this.ref = ref;
    }

    public void setOutput(String... input) {
        for (String line : input) {
            this.output.append(line);
        }
    }

}
