package com.nerdcorn.myth.data.drivers;

/**
 * Created by William Cliffe on 4/21/2017.
 */
abstract public class DataDriver {
    private String name;
    private DataScheme scheme;

    /**
     * Every data driver must have a connection method
     * @param args
     */
    abstract public void connect(String... args);

    /**
     *
     * @param scheme
     */
    public void setScheme(DataScheme scheme) {
        this.scheme = scheme;
    }

    public String getName() {
        return this.name;
    }

    public DataScheme getScheme() {
        return this.scheme;
    }

}
