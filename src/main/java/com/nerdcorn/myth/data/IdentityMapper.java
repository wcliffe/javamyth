package com.nerdcorn.myth.data;

import com.nerdcorn.myth.data.drivers.DataDriver;
import com.nerdcorn.myth.model.AbstractModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class IdentityMapper {
    private Map<String, DataDriver> driverList = new HashMap<>();
    private ArrayList<AbstractModel> models = new ArrayList<>();

    public IdentityMapper(DataDriver... driver) {
        for (DataDriver driverEntry : driver) {
            this.driverList.put(driverEntry.getName(), driverEntry);
        }
    }

    public static IdentityMapper getInstance(DataDriver... driver) {
        return new IdentityMapper(driver);
    }

    public ArrayList<AbstractModel> loadAll(AbstractModel model) {
        DataScheme dataScheme = model.getScheme();

        ArrayList<DataArtifact> artifacts = driver.setScheme(dataScheme).loadItems();
        for (DataArtifact artifact : artifacts) {
            this.models.add(artifact.toModel());
        }
    }
}
