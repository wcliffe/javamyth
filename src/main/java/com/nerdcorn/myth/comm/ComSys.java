package com.nerdcorn.myth.comm;

import com.nerdcorn.myth.GameServer;
import com.nerdcorn.myth.data.IdentityMapper;
import com.nerdcorn.myth.model.channel.Channel;
import com.nerdcorn.myth.model.user.User;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class ComSys {
    private IdentityMapper channelIdentityMapper;

    public ComSys(IdentityMapper channelIdentityMapper) {
        this.channelIdentityMapper = channelIdentityMapper;
    }

    public void createChannel(User user, String name, String description) {
        Channel channel = new Channel(name, user);
        channel.setDescription(description);

        GameServer.addChannel(channel);
    }

    public void loadChannels() {
        for (Channel channel : this.channelIdentityMapper.loadAll()) {
            GameServer.addChannel(channel);
        }
    }
}
