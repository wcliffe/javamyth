package com.nerdcorn.myth;

import com.nerdcorn.myth.comm.ComSys;
import com.nerdcorn.myth.data.IdentityMapper;
import com.nerdcorn.myth.io.Server;
import com.nerdcorn.myth.model.channel.Channel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class GameServer {
    public static Map<String, Channel> channels = new HashMap<>();

    public static void main(String[] args) {
        System.out.println("Starting up...");

        // Loading channels
        IdentityMapper channelIdentityMapper = FactoryBuilder
                .buildInstance(
                        IdentityMapper
                                .getInstance(
                                        new DataDriver(RedisCacheDataDriver),
                                        new DataDriver(MongoDBDataDriver)
                ));
        ComSys comSys = new ComSys(channelIdentityMapper);

        new Server();
    }

    public static void addChannel(Channel channel) {
        GameServer.channels.put(channel.getId(), channel);
    }
}
