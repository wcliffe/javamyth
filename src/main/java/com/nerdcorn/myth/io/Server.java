package com.nerdcorn.myth.io;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class Server {
    public Server() {
        setup(5005);
    }

    public void setup(int port) {
        ExecutorService executor = null;
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            executor = Executors.newFixedThreadPool(5);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                Runnable worker = new RequestHandler(clientSocket);
                executor.execute(worker);
            }
        } catch (Exception exception) {
            // Do log stuff here.
            System.out.println(exception.getMessage());
        } finally {
            if (executor != null) {
                executor.shutdown();
            }
        }

    }
}
