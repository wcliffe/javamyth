package com.nerdcorn.myth.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by William Cliffe on 4/20/2017.
 */
public class RequestHandler implements Runnable {
    private final Socket client;

    public RequestHandler(Socket client) {
        this.client = client;
    }

    public void run() {

        try (BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
             BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));) {
            System.out.println("Thread started with name: " + Thread.currentThread().getName());

            String line;
            while ((line = in.readLine()) != null) {
                line = line.replaceAll("[^A-za-z0-9 ]", "");
                System.out.println("Received message from " + Thread.currentThread().getName() + " : " + line);
                writer.write("You entered: " + line);
                writer.newLine();
                writer.flush();
            }
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

}
